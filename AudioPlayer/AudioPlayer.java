package AudioPlayer;

import java.util.*;
import java.io.*;

/** Audio Player Setup For Playing Harry Potter Theme Song In The Background */
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AudioPlayer
{

	Clip clip;

	AudioInputStream audioInputStream;
	String filePath = "harry_potter_loop.wav";

	// constructor to initialize streams and clip
	public AudioPlayer()
		throws UnsupportedAudioFileException,
		IOException, LineUnavailableException
	{
		// create AudioInputStream object
		audioInputStream =
				AudioSystem.getAudioInputStream(new File(this.filePath).getAbsoluteFile());

		// create clip reference
		clip = AudioSystem.getClip();

		// open audioInputStream to the clip
		clip.open(audioInputStream);

		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	// Method to play the audio
	public void play()
	{
		//start the clip
		clip.start();
	}
}