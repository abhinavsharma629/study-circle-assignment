Logic:
-----------
Created a Trie for the following sentence, as it will save space as well as fast query on word finding.
For finding all words, I  manupulated the trie structure, and at the ending of each word i kept an array of positions of where the word starts.

This also kept track of the . as well as " which eventually were helpful in taking easily extracting the sentence start and ends marked by . and conversations start and end marked by "

As its a well known fact that if a sentence starts, it will end too according to the English grammer rules, so the consecutive . mark the start and end position of the sentence,
and the same goes with conversations also, so thus i easily extracted the conversations and sentences from the para.

Now as i have the conversations start and end indexes as well as the indexes of the word, then i just used a range query to find out the total number of words that lie in a conversation, and if the
count > 0 then print the conversation.

For the pronouns:
As we know that the proper nouns start with capital letters, so first i made a regrex expression to take out all the words that start with capital letters
in the sentence. After finding all the eligible words, which definitely contains all the proper nouns, but we need to reject all the conjuctions, pronouns, adverbs etc.
So for my usecase i hardcoded most of them, but its not an appropriate solution.
We can use CoreNLP Library for finding out proper nouns in the sentence, which internally uses stemming of words, and finding their bases.
But its server API works very slowly, unless you have it installed in your machine, so i refrained from using it.
I also thought of creating my own server, and tried a lot, but the size of nltk package is a lot both in CoreNLP as well as nltk python.
