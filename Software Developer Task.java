import AudioPlayer.*;
import HarryPotterSentenceManupulation.*;

import java.util.*;

/** Chapter Reading Main Class */
class ChapterReading{
	public static void main(String[] args)
	{
		try
		{
			AudioPlayer audioPlayer = new AudioPlayer();
			audioPlayer.play();
		}
		catch (Exception ex)
		{
			System.out.println("Error with playing sound.");
			ex.printStackTrace();
		}

		try{
			Scanner sc = new Scanner(System.in);

			/** Harry Potter class Object */
			HarryPotter hp = new HarryPotter();

									/** Task 1 */

			/** Task 1. A */
			System.out.print("Enter a Word to Search In the file : ");
			String wordSearch = sc.next();
			hp.searchWordCount(wordSearch);

			/** Task 1. B */
			System.out.println("Listing all Sentences in which the current Word ocuurs...\n");
			hp.searchWordSentences(wordSearch);

			/** Task 1. C */
			System.out.println("Listing all Conversations in which the current Word ocuurs...\n");
			hp.searchWordConversations(wordSearch);

			/** ---------------------- Task 1 Ends ------------------- */



										/** Task 2 */
			System.out.println("Listing all Conversations...\n");
			hp.listAllConversations();

			/** ---------------------- Task 2 Ends ------------------- */



										/** Task 3 */
			System.out.println("Listing all Proper Nouns in the excerpt...\n");
			hp.listAllProperNouns();

			/** ---------------------- Task 3 Ends ------------------- */

		}

		catch (Exception ex)
		{
			System.out.println("Some Unexpected Error Occured!! ");
			ex.printStackTrace();
		}
	}
}
