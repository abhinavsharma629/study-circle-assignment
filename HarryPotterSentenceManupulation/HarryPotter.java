package HarryPotterSentenceManupulation;

import TrieCreation.Trie;

import java.util.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* Sentence Task Performance */
public class HarryPotter{

	String inputReadingFilePath = "TextFile.txt"; // default file name
	StringBuilder story;
	Trie trie;

	public HarryPotter(){ // Constructor dealing with reading and creating file trie
		System.out.println("Hello I am Your very own Harry Potter!!\n");
		System.out.println("Detecting Input File...\n");
		try{
			FileReader freader = new FileReader(this.inputReadingFilePath);
			BufferedReader br = new BufferedReader(freader);

			story = new StringBuilder();
			String line = null;
			System.out.println("Processing Input File...");
			while((line = br.readLine()) != null){
				story.append(line);
				story.append("\n");
			}
		}
		catch(Exception e){
			System.out.println("No input file exists!!\n");
		}
		finally{
			trie = new Trie();
			trie.insertSentence(story);
		}
		System.out.println("\n---------------------------------------\n");
	}


	/** ------------------------------------------------------------------------------------------------------------------------
											Task 1.1 : How many times does the word occur in the excerpt
	*/
	// TIME COMPLEXITY : O(depth(Trie))
	public void searchWordCount(String word){
		ArrayList<Integer> counts = trie.searchWord(word);
		System.out.println("\n"+word+" appears "+counts.size()+" times in the given excerpt!!");
		System.out.println("-----------------------------------------------------------\n");
		return ;
	}


	/**------------------------------------------------------------------------------------------------
	 					TASK 1.2 : List all sentences in which the word appears

	Finding position for the sentence start */
	// TIME COMPLEXITY :O(log(Number Of Sentences))
	public int findJustLowerPosition(int wordStartPos, ArrayList<Integer> positions){
		int l = 0;
		int r = positions.size() - 1;
		while(l <= r){
			int mid = (l + r) / 2;
			if(positions.get(mid) >= wordStartPos){
				r = mid - 1;
			}
			else{
				l = mid + 1;
			}
		}
		return r;
	}

	/** Get Valid sentence for the current start and end Positions */
	// TIME COMPLEXITY : O(log(Number Of Sentences) + len(Sentence))
	public String getString(ArrayList<Integer> wordPositions, ArrayList<Integer> otherPositions, int i){

		int lowerPos = findJustLowerPosition(wordPositions.get(i), otherPositions);
		int higherPos = lowerPos + 1 >= otherPositions.size() ? story.length() - 1 : otherPositions.get(lowerPos + 1);

		/** adjusting positions for overflows */
		if(lowerPos < 0){
			lowerPos = 0;
		}
		else{
			lowerPos = otherPositions.get(lowerPos);
		}

		String tempStringCopy = story.substring(lowerPos == 0 ? lowerPos : lowerPos + 1, higherPos);
		int j = 0;
		while(j < tempStringCopy.length() && (trie.isNotAlphabet(tempStringCopy.charAt(j)) && tempStringCopy.charAt(j) != '"')){
			j += 1;
		}
		if(j < tempStringCopy.length()){
			return tempStringCopy.substring(j, tempStringCopy.length());
		}
		return "";
	}


	// TIME COMPLEXITY : O(2 * depth(Trie) + len(Word Matches) * (log(Number Of Sentences) + len(Sentence)))
	public void searchWordSentences(String word){
		ArrayList<Integer> dotPositions = trie.searchWord(".");
		ArrayList<Integer> wordPositions = trie.searchWord(word);
		int count = 1;

		for(int i = 0 ; i < wordPositions.size() ; i++){
			String str = getString(wordPositions, dotPositions, i);
			if(str.length() > 0){
				System.out.println("Sentence "+count+" : "+str+"\n");
				count += 1;
			}
		}
		System.out.println("-----------------------------------------------------------\n");
		return ;
	}


	/**------------------------------------------------------------------------------------------------
	 					TASK 1.3 : Is the word mentioned in a conversation? Which ones and how many times
	*/

	/** Finding the word count within the given conversation limits */
	public int findWordCountWithinLimits(int startPos, int endPos, ArrayList<Integer> positions){
		int count = 0;
		for(int i = 0 ; i < positions.size() ; i++){
			if(positions.get(i) >= startPos && positions.get(i) <= endPos){
				count += 1;
			}
			else if(positions.get(i) > endPos){
				break;
			}
		}
		return count;
	}

	/** List All conversations in which the given word lies, and also the word count */
	public void searchWordConversations(String word){
		ArrayList<Integer> quotesPositions = trie.searchWord("\"");
		ArrayList<Integer> wordPositions = trie.searchWord(word);
		int count = 1;

		for(int i = 0 ; i < quotesPositions.size() ; i += 2){
			/** If there is a conversation taking place then the quotes array contains atleast 2 quotes marking the start and end of a VALID comversation
			Based on the above fact the start position of the conversation always lies on even index and the end position on the odd index. */

			if(i + 1 < quotesPositions.size()){
				int totalWords = findWordCountWithinLimits(quotesPositions.get(i), quotesPositions.get(i + 1), wordPositions); /** Finding if there are any word matches within the given conversation */
				if(totalWords != 0){
					System.out.println("Conversation "+count+" : "+story.substring(quotesPositions.get(i), quotesPositions.get(i + 1) + 1));
					System.out.println("Total Words In This Conversation : "+totalWords+"\n");
					count += 1;
				}
			}
			else{
				// not correctly formed conversation
				int totalWords = findWordCountWithinLimits(quotesPositions.get(i), story.length() - 1, wordPositions);
				if(totalWords != 0){
					System.out.println("Conversation "+count+" : "+story.substring(quotesPositions.get(i), story.length()));
					System.out.println("Total Words In This Conversation : "+totalWords+"\n");
					count += 1;
				}
			}
		}
		System.out.println("-----------------------------------------------------------\n");
		return ;
	}


	/**------------------------------------------------------------------------------------------------
	 														TASK 2 : Listing All Conversations
	*/
	public void listAllConversations(){
		ArrayList<Integer> quotesPositions = trie.searchWord("\""); // get positions of all quotes
		for(int i = 0 ; i < quotesPositions.size() ; i += 2){
			/** If there is a conversation taking place then the quotes array contains atleast 2 quotes marking the start and end of a VALID comversation
			Based on the above fact the start position of the conversation always lies on even index and the end position on the odd index. */

			if(i + 1 < quotesPositions.size()){
				System.out.println(story.substring(quotesPositions.get(i), quotesPositions.get(i + 1) + 1));
			}
			else{
				// not correctly formed conversation
				System.out.println(story.substring(quotesPositions.get(i), story.length()));
			}
		}
		System.out.println("-----------------------------------------------------------\n");
	}


	/**------------------------------------------------------------------------------------------------
		TASK 3 :  Find all proper nouns in the excerpt, and what is the logic in determining the proper noun?
	*/
	public boolean isNotConjuction(String word){
		if(word.equalsIgnoreCase("it") || word.equalsIgnoreCase("or") || word.equalsIgnoreCase("and") || word.equalsIgnoreCase("not") || word.equalsIgnoreCase("either") ||
		word.equalsIgnoreCase("do") || word.equalsIgnoreCase("don't") || word.equalsIgnoreCase("he") || word.equalsIgnoreCase("she") || word.equalsIgnoreCase("they") ||
		word.equalsIgnoreCase("are") ||  word.equalsIgnoreCase("is") || word.equalsIgnoreCase("but") || word.equalsIgnoreCase("i") ||
		word.equalsIgnoreCase("in") || word.equalsIgnoreCase("the") || word.equalsIgnoreCase("when") || word.equalsIgnoreCase("his") ||
		word.equalsIgnoreCase("her") || word.equalsIgnoreCase("uncle") || word.equalsIgnoreCase("aunt") || word.equalsIgnoreCase("ten")){
			return false;
		}
		return true;
	}


	/**
	public void findProperNoun(String arr[]){
		for(int j = 0 ; j < arr.length; j++){
			for(int k = 0 ; k < arr[j].length() ; k++){
				if(Character.isLetter(arr[j].charAt(k))){
					if(Character.isUpperCase(arr[j].charAt(k)) && isNotConjuction(arr[j])){
						System.out.println(arr[j]);
					}
				}
			}
		}
	}*/

	public void listAllProperNouns(){
		String toStr = story.toString();

		/** Logic Used : As we know that the proper nouns start with capital letters, so first i made a regrex expression to take out all the words that start with capital letters
			in the sentence. After finding all the eligible words, which definitely contains all the proper nouns, but we need to reject all the conjuctions, pronouns, adverbs etc.
			So for my usecase i hardcoded most of them, but its not an appropriate solution.
			We can use CoreNLP Library for finding out proper nouns in the sentence, which internally uses stemming of words, and finding their bases.
			But its server API works vry slowly, unless you have it installed in your machine, so i refrained from using it.
		*/
		String pattern = "[A-Z]+[a-z]*('s)?";
		// Create a Pattern object
		Pattern r = Pattern.compile(pattern);

		// Now create matcher object.
		Matcher matcher = r.matcher(toStr);
		List<String> li = new ArrayList<> ();
		while(matcher.find()) {
			if(isNotConjuction(matcher.group().toLowerCase()))
				System.out.println(matcher.group());
		}
		System.out.println("-----------------------------------------------------------\n");
		return ;
	}

}

/** ---------------xxxxxxxxxxxxxxxxxxxxx--------------- */

