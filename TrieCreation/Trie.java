package TrieCreation;

import java.util.*;

/** Trie for fast and efficient word search */
public class Trie{
	Trie head;
	HashMap<Character, Trie> hmap;
	boolean isEnd;
	ArrayList<Integer> positions;

	public Trie(){
		hmap = new HashMap<> ();
		head = this;
		isEnd = false;
		positions = new ArrayList<> ();
	}

	public boolean isNotAlphabet(char ch){ // determine if a character alphabet of not
		if((int)(ch) >= 65 && (int)(ch) <= 90){
			return false;
		}
		else if((int)(ch) >= 97 && (int)(ch) <= 122){
			return false;
		}
		else if((int)(ch) == 39){
			return false;
		}
		return true;
	}

	public void insertSentence(StringBuilder story){
		int i, startIndex = -1;
		for(i = 0 ; i < story.length() ; i++){
			if(startIndex == -1){
				if(isNotAlphabet(story.charAt(i))){
					insertInTrie(String.valueOf(story.charAt(i)), i); //  inserting non alphabatic characters
				}
				else{
					startIndex = i;
				}
			}
			else{
				if(isNotAlphabet(story.charAt(i))){
					insertInTrie(String.valueOf(story.charAt(i)), i); //  inserting non alphabatic characters
					insertInTrie(story.substring(startIndex, i), i - 1); //  inserting words
					startIndex = -1;
				}
				else{
					continue;
				}
			}
		}
		if(startIndex != -1){
			insertInTrie(story.substring(startIndex, i), i - 1); //  inserting words
		}
	}

	public void insertInTrie(String str, int pos){ // inserting words in the trie
		Trie headCpy = head;

		for(int i = 0 ; i <str.length() ; i++){
			if(headCpy.hmap.containsKey(Character.toLowerCase(str.charAt(i)))){
				headCpy = headCpy.hmap.get(Character.toLowerCase(str.charAt(i)));
			}
			else{
				Trie tempNode = new Trie();
				headCpy.hmap.put(Character.toLowerCase(str.charAt(i)), tempNode);
				headCpy = tempNode;
			}
		}
		headCpy.isEnd = true;
		headCpy.positions.add(pos);
	}

	public ArrayList<Integer> searchWord(String word){ //  searching for a word in the excerpt
		Trie headCpy = this.head;

		for(int i = 0 ; i < word.length() ; i++){

			if(headCpy.hmap.containsKey(Character.toLowerCase(word.charAt(i)))){
				headCpy = headCpy.hmap.get(Character.toLowerCase(word.charAt(i)));
			}
			else{
				return new ArrayList<> ();
			}
		}

		if(headCpy.isEnd){
			return headCpy.positions;
		}

		return new ArrayList<> ();
	}
}